﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BasicCounterTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(1,BasicCounterLib.BasicCounterClass.Incrementation(0));
            Assert.AreEqual(-1, BasicCounterLib.BasicCounterClass.Decrementation(0));
            Assert.AreEqual(0, BasicCounterLib.BasicCounterClass.RAZ(1));
        }
    }
}
