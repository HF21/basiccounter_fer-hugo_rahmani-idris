﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterLib
{
    public class BasicCounterClass

    {
        protected int total = 0;

        public static int Incrementation(int total)
        {
            total = total + 1;
            return total;
        }

        public static int Decrementation(int total)
        {
            total = total - 1;
            return total;
        }

        public static int RAZ(int total)
        {
            total = 0;
            return total; 
        }
    }
}
